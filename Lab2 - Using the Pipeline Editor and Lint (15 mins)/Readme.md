## Lab2 - Pipeline Editor (15 mins)

The pipeline editor is the primary place to edit the GitLab CI/CD configuration in the .gitlab-ci.yml file in the root of your repository. To access the editor, go to CI/CD > Editor.

From the pipeline editor page you can:

- Validate your configuration syntax while editing the file.
- Do a deeper lint of your configuration, that verifies it with any configuration added with the include keyword.
- See a visualization of the current configuration.
- View an expanded version of your configuration.
- Commit the changes to a specific branch.


In this lab we will experiance the Pipeline Editor


### Exercise 1  - Create a basic CI configuration using the Pipeline editor 

1. Make sure you followed the cleanup step in `lab1`.
2. Create a new `.gitlab-ci.yml` in the root of the repository, leave the file empty, and commit it to the `master` branch. 
3. Open the pipeline editor (CICD->Editor), and create three stages `build`, `test` and `deploy`, in each stage add a jobs with a same name of the stage, and a script that echo `hello-world`.
4. Check the Visualize tab, and the Lint tab.
5. Remove the stages definition entirly, and add `include:` 
                                    `- template: Auto-DevOps.gitlab-ci.yml`.
6. Check the `View merged YAML` tab. 
7. In the merged YAML, serach for `hello-world`, check that your jobs have been merged succcesfully with the template jobs.  

### Cleanup 

1. Rename the CI configuration `.gitlab-ci.yml` to `.lab2-gitlab-ci.yml` , you can use this file for reference for the next labs, also use it in future whenever you need CI code examples.
2. Commit this change to `master` branch



