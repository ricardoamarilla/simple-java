# Lab4 - Pipelines and Merge Requests (20 mins)

### In this lab we will cover the following topics:

- Variables 
- Rules 

### Variables and the rules label - Lesson 

[Watch this video ](https://drive.google.com/file/d/1FsapP5_kAPycQje4v_u0AOH9P2pCG4tG/view?usp=sharing)

[Pipelines and Merge Requests](https://docs.google.com/presentation/d/1R4KLFTWFMLaRL_hdIxwgRp37CeYtqQ8Krbf112hbRFo/edit?usp=sharing)

## Exercise 1  - The Branch and merge request pipeline 

1. Open the Pipelines page (from the left menu, open CI/CD->Pipelines)
2. The Pipeline page presents pipeline history list 
3. Identify via the icon of the first pipeline in the list, if it is a branch pipeline or MR pipeline, check the branch name and the commit message 
4. Create a Merge Request Pipeline, check the pipeline icon, and the MR id in the pipelines page.

## Exercise 2 - Pipeline for Merged Results and Merge Trains

1. What is pipeline for Merged results? 
2. What is a Merge Train, which problem it solves? 

### Cleanup 

1. Rename the CI configuration `.gitlab-ci.yml` to `.lab4-gitlab-ci.yml` , you can use this file for reference for the next labs, also use it in future whenever you need CI code examples.
2. Commit this change to `master` branch

